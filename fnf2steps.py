#Punkte sind Arrays mit 2 Werten:
#0 ≙ X-Koordinate
#1 ≙ Y-Koordinate

#step_sequence ist ein ein zweidimensionales Array aus Integern, jeweils 3 pro Zeile:
#0 ≙ X-Achse
#1 ≙ Y-Achse
#2 ≙ Z-Achse

#Die Werte für die X- und Y-Achse in step_sequence bedeuten:
#-1 ≙ 1 Schritt rückwärts
#0 ≙ Kein Schritt
#1 ≙ Schritt vorwärts

#Bei der Z-Achse gibt es nur 2 Befehle:
#0 ≙ Zustand ändern
#1 ≙ Zustand beibehalten


from parseFNF import fnf_file


#global_scale gibt das Verhältnis von einem cm in der FNF Datei bzw. in dem dadurch überall im Code verwendeten Koordinatensystem zu einem cm auf dem Papier an. Ich bin mir nicht sicher ob das irgendwann benötigt wird aber ich habe es lieber mal direkt eingebaut, denn solange der Wert 1 ist stört es nicht.
global_scale = 1

#steps_per_mm gibt die Anzahl der Schritte an, die benötigt werden um einen mm in eine Richtung abzufahren
steps_per_mm = 1


def goto_point(difference, scale):
    #Bresenham Algorithmus um eine Linie zu rasterisieren
    goto_point_step_seq = []
    dx = abs(round(difference[0] * scale * steps_per_mm))
    dy = abs(round(difference[1] * scale * steps_per_mm))

    if difference[0] > 0: signX = 1
    else: signX = -1
    if difference[1] > 0: signY = 1
    else: signY = -1

    if dx >= dy:
        fasterDirection = dx
        slowerDirection = dy
        fasterX = True
    else:
        fasterDirection = dy
        slowerDirection = dx
        fasterX = False

    error = fasterDirection/2

    for x in range(0, fasterDirection):

        if error - slowerDirection < 0:
            goto_point_step_seq.append([signX, signY, 0])
            error += fasterDirection

        else:
            if fasterX:
                goto_point_step_seq.append([signX, 0, 0])
            else:
                goto_point_step_seq.append([0, signY, 0])

            error -= slowerDirection

    return goto_point_step_seq



def fnf2steps(filename, scale):
    step_sequence = []

    #TODO: Fahre an 0/0. Es muss entweder einen festen Befehl geben um dem Gerät mitzuteilen, dass es zum Nullpunkt fahren soll oder es muss vor jeder Aktion zu 0/0 fahren.

    f = fnf_file(filename)

    #Zuerst muss man sich von 0/0 zum Startpunkt der Datei begeben.
    step_sequence.append(goto_point(difference=f.getInitial(), scale=scale))
    prevTip = False

    for i in range(f.getStepCount()):
        #Wenn sich der Zustand des Stiftes ändert (z.B. aufgedrückt --> nicht aufgedrückt) wird dies wie oben erwähnt durch eine 1 an dritter Stelle vermerkt.
        if f.getTip(i) != prevTip:
            step_sequence.append([0,0,1])

        prevTip = f.getTip(i)

        for step in goto_point(difference=f.getStep(i), scale=scale):
            step_sequence.append(step)

    return(step_sequence)


#Der folgende Code existiert nur zu Testzwecken. Er generiert eine Datei die man mit testplotter.py nutzen kann. Als ich testplotter.py geschrieben habe wusste ich noch nicht, wie die Ausgabe von diesem Programm aussehen würde, deshalb so eine merkwürdige Formatierung.
f = fnf_file("A.fnf")
o = open("output.txt", "w")
prevTip = False
for i in range(f.getStepCount()):
    if f.getTip(i) != prevTip:
        o.write("0,0,1")
        o.write("\n")

    prevTip = f.getTip(i)

    for x in goto_point(f.getStep(i), global_scale):
        o.write(str(x[0])+","+str(x[1])+","+str(x[2]))
        o.write("\n")

o.close()
