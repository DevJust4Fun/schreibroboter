class fnf_file:
	
	def __init__(self, path):
		self.location = path
	
	#types: 0-string, 1-int, 2-float, 3-bool
	def __getKeyword(self, key, type):
		f = open(self.location, "r")
		res = 0
		for l in f.readlines():
			if(l.startswith(key)):
				res = l[len(key)+1:-1]
		f.close()
		if(type == 1):
			res = int(res, 10)
		elif(type == 2):
			res = float(res)
		elif(type == 3):
			res = bool(res)
		return res
	
	def getLocation(self):
		return self.location
	
	def getName(self):
		return self.__getKeyword("name", 0)
		
	def getDescription(self):
		return self.__getKeyword("desc", 0)
	
	def getStepCount(self):
		return self.__getKeyword("delta", 0).count(";") + 1
	
	def getInitial(self):
		return (self.__getKeyword("x1", 2),self.__getKeyword("y1", 2))
		
	def getStep(self, i):
		current = self.__getKeyword("delta", 0)[1:-1].split(";")[i][1:-1]
		return (float(current.split(",")[0]), float(current.split(",")[1]))
		
	def getTip(self, i):
		line = self.__getKeyword("delta", 0)
		while(line.split(";")[i].count(",") < 2):
			i = i - 1
		current = line.split(";")[i][1:-1]
		return bool(int(current.split(",")[2], 10))